<?php
/**
 * Template part for post-header with square image
 *
 * Uses ACF group to get text in header
 *
 * @package hum-v7-core
 */
?>

<header class="page-header page-header--square">

  <div class="block-body wrap">

    <div class="grid grid--50">

      <div class="block block--title">

        <?php
        // breadcrumbs
        get_template_part( 'template-parts/site/yoast', 'breadcrumbs' );

        // title
        the_title( '<h1 class="post-title">', '</h1>' );

        if ( have_rows( 'page_intro_group' ) ) {
          while ( have_rows( 'page_intro_group' ) ) {

            the_row();
            include( locate_template( 'template-parts/acf/partials/text.php') );
            include( locate_template( 'template-parts/acf/partials/link__repeater.php') );

          }
        }
        ?>

      </div>

      <div class="block block--image">

        <?php
        // post image
        if ( has_post_thumbnail() ) {
          echo '<div class="post-featured-image">'; the_post_thumbnail( 'featured-sq' ); echo '</div>';
        }
        ?>

      </div>

    </div>

  </div>

</header>
