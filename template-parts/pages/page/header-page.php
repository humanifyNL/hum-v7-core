<?php
/**
 * Template part for standard page-header
 *
 * @package hum-v7-core
 */
?>

<header class="page-header">

  <div class="wrap">

    <div class="grid">

      <div class="block block--text block--title">

        <?php
        // breadcrumbs
        get_template_part( 'template-parts/site/yoast', 'breadcrumbs' );

        // title
        the_title( '<h1 class="page-title">', '</h1>' );

        // text
        if ( have_rows( 'page_intro_group' ) ) {
          while ( have_rows( 'page_intro_group' ) ) {

            the_row();
            include( locate_template( 'template-parts/acf/partials/text.php') );
            include( locate_template( 'template-parts/acf/partials/link__repeater.php') );

          }
        }
        ?>

      </div>

    </div>

  </div>

</header>
