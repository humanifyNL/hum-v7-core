<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 * @package hum-v7-core
 */
?>

<section class="error-404 not-found ">

  <header class="page-header">

    <h1 class="page-title wrap"><?php esc_html_e( 'Deze pagina URL bestaat niet.', 'hum-base' ); ?></h1>

  </header>

  <div class="page-content">

    <section class="row row--content has-bg no-grid">

      <div class="wrap">

        <p><?php esc_html_e( 'Dit is natuurlijk niet de bedoeling, maar het kan een keer gebeuren. Het kan zijn dat de pagina verwijderd is, of dat de link is veranderd. Hier wat opties om je verder te helpen.', 'hum-base' ); ?></p>

        <?php
        get_search_form();
        ?>

      </div>

    </section>


    <section class="row">

      <div class="wrap">

        <div class="grid grid--50 block-body">

          <div class="block block--left">

            <h3 class="block__title"><?php esc_html_e( 'Categorieën', 'hum-base' ); ?></h3>

            <?php
            $args_404_c = array(
              'taxonomy' => 'category',
              'post_type'=> array( 'post'),
              'orderby' => 'name',
              'order' => 'ASC',
            );
            $cats_404 = get_terms( $args_404_c );

            echo '<ul class="block__list">';

            foreach ( $cats_404 as $cat ) {
              $cat_link = get_tag_link( $cat->term_id );

              echo '<li><a href="'.$cat_link.'" class="'.$cat->slug.' tag" rel="nofollow">'.$cat->name.'<span class="tag__amount"> &#40;'.$cat->count.'&#41;</span></a></li>';
            }

            echo '</ul>';
            ?>

          </div>

          <div class="block block--right">

            <h3 class="block__title"><?php esc_html_e( 'Tags', 'hum-base' ); ?></h3>

            <?php
            $args_404_t = array(
              'taxonomy' => 'post_tag',
              'post_type'=> array( 'post'),
              'orderby' => 'name',
              'order' => 'ASC',
            );
            $tags_404 = get_terms( $args_404_t );

            echo '<ul class="block__list">';

            foreach ( $tags_404 as $tag ) {
              $tag_link = get_tag_link( $tag->term_id );

              echo '<li><a href="'.$tag_link.'" class="'.$tag->slug.' tag" rel="nofollow">'.$tag->name.'<span class="tag__amount"> &#40;'.$tag->count.'&#41;</span></a></li>';
            }

            echo '</ul>';
            ?>

          </div>

        </div>

      </div>

    </section>

  </div>

</section><!-- .error-404 -->
