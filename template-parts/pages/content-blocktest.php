<?php
/**
 * Content styleguide
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/pages/page/header', 'page' );
	?>

	<div class="page-content">

		<?php
		$block_types = array(
			'default',
			'style-1',
			'style-2',
		);

		foreach( $block_types as $block_type ) {

			?>
			<section class="row row--section blocks">

				<div class="block-body wrap">

					<div class="grid grid--50">

						<div class="block <?php echo $block_type; ?>">

							<h3 class="block__title">Block <?php echo $block_type; ?></h3>

							<div class="block__text is-excerpt">
								<p>This is a demo block text which is used to describe the linked page content or an action that can be done. These texts are usually limited in the amount of words available.</p>
							</div>

							<div class="block__footer">
								<a href="#" class="click btn button">Fake link</a>
							</div>

						</div>

					</div>

				</div>

			</section>
			<?php
		}


		?>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
