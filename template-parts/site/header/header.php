<?php
/**
 * Header
 *
 * @package hum-v7-core
 */

$enable_lang = get_field( 'enable_language_select', 'option' );
?>

<header class="header header--site" id="site-header">

	<!-- top bar -->
	<div class="header--top">

	  <div class="wrap wrap--header">

	    <?php
			include( locate_template( 'template-parts/site/header/header__links.php' ) );

			include( locate_template( 'template-parts/site/header/header__lang.php' ) );
	    ?>

	  </div>

	</div>

	<div class="header--main">

		<div class="wrap wrap--header">

			<!-- left side -->
			<div class="header--left">

				<?php
				include( locate_template( 'template-parts/site/header/header__branding.php' ) );
				?>

			</div>

			<!-- right side -->
			<div class="header--right">

				<?php
				include( locate_template( 'template-parts/site/menu/menu-main.php' ) );
				?>

			</div>

		</div>
		
	</div>

</header>
