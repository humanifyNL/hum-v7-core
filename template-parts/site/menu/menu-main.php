<?php
/**
 * Main menu
 *
 * @package hum-v7-core
 */
?>

<nav id="site-navigation" class="nav--main" aria-label="<?php esc_attr_e( 'Primary Menu', 'hum-base' ); ?>" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">

  <h2 class="screen-reader-text"><?php esc_html_e( 'Primary Menu', 'hum-base' ); ?></h2>
  <div class="menu-main__toggle"><button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'hum-base' ); ?></button></div>

  <?php
  wp_nav_menu( array(
    'theme_location' => 'primary',
    'menu_id'        => 'primary-menu',
    'menu_class'     => 'menu menu--main',
    'depth'          => 4,
    'container_class'=> 'wrap-menu',
  ) );
  ?>

</nav>
