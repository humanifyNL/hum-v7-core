<?php
/**
 * Slider with swiper.js
 *
 * ACF field: group_5f0b0e81748e3
 *
 * @package hum-v7-core
 */

if ( have_rows( 'swiper_slides_txt' ) ) {

  ?>
  <div class="block__img block__img--slider">

    <div class="swiper-wrapper">

      <?php
      while ( have_rows( 'swiper_slides_txt' ) ) {

        the_row();

        $slide_size = 'large';
        $slide_id = get_sub_field( 'swiper_slide_id' );

        echo '<div class="swiper-slide">';

          echo wp_get_attachment_image( $slide_id, $slide_size );

          // optional text
          $text_wysi = get_sub_field( 'text_slider' );
          if ( $text_wysi ) {

            echo '<div class="block__text">';

            echo $text_wysi;

            echo '</div>';

          }

        echo '</div>';

      }
      ?>

    </div>

  </div>
  <?php
}
