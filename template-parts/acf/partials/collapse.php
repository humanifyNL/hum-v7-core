<?php
/**
 * Collapsible list block
 *
 * ACF field: group_60212965217d1
 *
 * @package hum-v7-core
 */

if ( have_rows( 'collapse_repeater' ) ) {
?>

  <ul class="block__list list--collapse">

    <?php
    while ( have_rows( 'collapse_repeater' ) ) {

      the_row();

      $c_question = get_sub_field( 'collapse_question' );
      $c_answer = get_sub_field( 'collapse_answer' );
      $c_link = get_sub_field( 'collapse_link' );
      $c_link_title = get_sub_field( 'collapse_link_title' );


      echo '<li class="block__item collapse">';

        if ( $c_question ) {

          echo '<div class="block__frame collapse--wrap">';

            echo '<h4 class="block__title">'.$c_question.'</h4>';

            if ( $c_answer ) {

              echo '<div class="collapse--card">';

                echo '<div class="block__text">';

                  echo $c_answer;

                echo '</div>';

                if( $c_link ) {

                  echo '<div class="block__footer">';

                    echo '<a href="'.$c_link.'" class="block__btn btn button--wired'; echo '">';
                    if ( $c_link_title ) { echo $c_link_title; } else { echo 'Lees meer'; }
                    echo '</a>';

                  echo '</div>';
                }

              echo '</div>';
            }

          echo '</div>';
        }

      echo '</li>'; // block

    }
    ?>

  </ul>
<?php
}
