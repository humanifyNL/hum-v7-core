<?php
/**
 * Link partial
 *
 * ACF field: group_5f087f17ba886
 *
 * @package hum-v7-core
 */

$link = get_sub_field( 'link_page_post' );
$link_title = get_sub_field( 'link_title' );
$link_type = get_sub_field( 'link_type' );

if ( $link_type == 'block__link' ) {
  $link_class = 'block__link';
} else {
  $link_class = 'block__btn '.$link_type;
}

if ( $link ) {

  echo '<a class="btn '.$link_class.'" href="'.esc_url($link).'">';

  if ( $link_title ) {

    echo esc_html($link_title);

  } else {

    echo "Lees meer";

  }

  echo '</a>';
}
