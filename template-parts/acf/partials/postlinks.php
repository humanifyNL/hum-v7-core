<?php
/**
 * Postlinks repeater
 *
 * @package hum-v7-core
 */
?>

<div class="grid--previews <?php echo hum_grid_preview(); ?>">

  <?php
  if ( have_rows( 'postlinks_repeater') ) {

    while ( have_rows( 'postlinks_repeater') ) {

      the_row();

      // setup post type
      $post = get_sub_field( 'postlinks_post_object' );
      if ( $post ) {

        setup_postdata( $post );

        // vars
        $enable_custom_title = get_sub_field( 'enable_postlinks_title' );
        $enable_custom_descr = get_sub_field( 'enable_postlinks_descr' );
        $link_text = get_sub_field ( 'postlinks_link_text' );

        if ( $enable_custom_title ) {
          $block_title = get_sub_field( 'postlinks_custom_title' );
        } else {
          $block_title = $post->post_title;
        }
        if ( $enable_custom_descr ) {
          $block_descr = get_sub_field( 'postlinks_custom_descr' );
        } else {
          $block_descr = get_the_excerpt( $post->ID );
        }

        $block_link = get_permalink();
        ?>

        <article id="post-<?php echo $post->ID; ?>" class="clickable preview preview--post">

          <?php
          $thumb = get_the_post_thumbnail( $post->ID,'medium' );

          echo '<div class="block__thumb">';
          if ( !$thumb ) { echo hum_default_img();
          } else { echo $thumb; }
          echo '</div>';
          ?>

          <div class="block__body">

            <?php
            if ( $block_title ) {
              echo '<h3 class="block__title">'.$block_title.'</h3>';
            }

            if ( $block_descr ) {
              echo '<div class="block__text is-excerpt"><p>'.$block_descr.'</p></div>';
            }

            if ( $block_link ) {
              ?>
              <div class="block__footer">

                <?php
                echo '<a href="'. $block_link . '" class="'. hum_button_class( 'post' ) .'">';
                if ( $link_text ) { echo $link_text; } else { echo 'Lees meer';}
                echo '</a>';
                ?>

              </div>
              <?php
            }
            ?>

          </div>

        </article>
        <?php
        wp_reset_postdata();
      }
    }
  }
  ?>

</div>
