<?php
/**
 * Flex content - page
 *
 * ACF field: group_5f06ea7d13be4
 *
 * @package hum-v7-core
 */

if ( have_rows( 'flex_contact' )) {

  while ( have_rows( 'flex_contact' )) {

    the_row();

    // layouts
    if ( get_row_layout() == 'text' ) {

      get_template_part( 'template-parts/acf/rows/row--text' );
    }

    if ( get_row_layout() == 'map' ) {

      get_template_part( 'template-parts/acf/rows/row--contact-map' );
    }

    if ( get_row_layout() == 'contact_links' ) {

      get_template_part( 'template-parts/acf/rows/row--contact-links' );
    }

    if ( get_row_layout() == 'contact_company' ) {

      get_template_part( 'template-parts/acf/rows/row--contact-company' );
    }

    if ( get_row_layout() == 'form' ) {

      get_template_part( 'template-parts/acf/rows/row--form' );
    }


  }

}
