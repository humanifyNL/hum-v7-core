<?php
/**
 * Contact links section
 *
 * @package hum-v7-core
 */
?>

<section class="row row--contact <?php echo hum_row_style(); ?>" <?php hum_row_img(); ?>>

  <div class="section-body wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      include( locate_template( 'template-parts/acf/blocks/block--text.php') );
      ?>

      <div class="block block--contact links <?php echo hum_block_style(); ?>">

        <?php
        include( locate_template( 'template-parts/acf/partials/contact-links.php') );
        ?>

      </div>

    </div>

  </div>

</section>
