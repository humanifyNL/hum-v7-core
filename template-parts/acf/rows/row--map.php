<?php
/**
 * Map section
 *
 * @package hum-v7-core
 */
$map_wide = get_sub_field( 'map_full_width' );
?>

<section class="row row--map <?php if ( $map_wide ) { echo 'is-wide';} echo hum_row_style(); ?>" <?php hum_row_img(); ?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      include( locate_template( 'template-parts/acf/blocks/block--map.php' ) );
      ?>

    </div>

  </div>

</section>
