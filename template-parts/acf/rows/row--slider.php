<?php
/**
 * Text section with image slider
 *
 * ACF field: group_5f0b0e81748e3
 *
 * @package hum-v7-core
 */
?>

<section class="row row--slider <?php echo hum_row_style(); ?>" <?php hum_row_img(); ?>>

  <div class="wrap">

    <div class="grid <?php echo hum_grid_section(); ?>">

      <?php
      include( locate_template( 'template-parts/acf/blocks/block--text.php') );
      include( locate_template( 'template-parts/acf/blocks/block--slider.php') );
      ?>

    </div>

  </div>

</section>
