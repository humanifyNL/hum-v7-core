<?php
/**
 * Block--text-r
 *
 * @package hum-v7-core
 */

if ( get_sub_field( 'text_wysi_r' ) ) {
  ?>
  <div class="block block--text <?php echo hum_block_style();?>">

    <?php
    include( locate_template( 'template-parts/acf/partials/text__wysi-r.php') );
    include( locate_template( 'template-parts/acf/partials/link__repeater-r.php') );
    ?>

  </div>
  <?php
}
