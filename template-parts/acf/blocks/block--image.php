<?php
/**
 * Block--image
 *
 * @package hum-v7-core
 */

if ( get_sub_field( 'image_array' ) ) {

  ?>
  <div class="block block--image <?php echo hum_block_style();?>">

    <?php
    include( locate_template( 'template-parts/acf/partials/image.php') );
    ?>

  </div>
  <?php

}
