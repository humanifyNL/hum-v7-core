<?php
/**
 * Query row - posts (with isotope)
 *
 * @package hum-v7-core
 */
?>

<section class="row row--previews posts has-isotope <?php echo hum_row_style(); ?>" <?php hum_row_img();?>>

  <div class="wrap">

    <?php
    include( locate_template( 'template-parts/singles/post/query-posts__iso.php' ) );
    ?>

  </div>

</section>
