<?php
/**
 * Display archive-type pages for posts by an author.
 *
 * @package hum-v7-core
 */

if ( have_posts() ) {

  // Queue the first post, that way we know
  // what author we're dealing with (if that is the case).
  // We reset this later so we can run the loop
  the_post();

	get_template_part( 'template-parts/pages/page/header', 'page__archive' );

  // Since we called the_post() above, we need to
  // rewind the loop back to the beginning so
  // we can run the loop properly, in full.
  rewind_posts();

  // If a user has filled out their description, show author bio on their entries.
  if ( get_the_author_meta( 'description' ) ) {

		echo '<div class="wrap">';
			echo '<div class="grid">';

      	get_template_part( 'template-parts/singles/author/block', 'author' );

			echo '</div>';
		echo '</div>';
  }

  ?>
	<section class="row row--previews author">

		<div class="wrap">

			<h2 class="row__title"><?php the_archive_title();?>'s posts:</h2>

      <div class="grid--previews">

        <?php
        while ( have_posts() ) {

          the_post();
          get_template_part( 'template-parts/singles/post/preview', 'post' );

        }

        hum_archive_page_nav();
				?>

			</div>

		</div>

	</section>
	<?php

}
