<?php
/**
 * The default template for displaying post content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php
  get_template_part( 'template-parts/singles/post/header', 'post' );
  ?>

  <div class="post-content">

    <div class="wrap">

      <div class="post-body">

        <?php
        if ( get_post_type( get_the_ID() ) == 'post' ) {
          get_template_part( 'template-parts/site/share-dialog');
        }

        if ( have_rows( 'flex_content_post' ) ) {
          get_template_part( 'template-parts/acf/flex-post' );
        } else {
          get_template_part( 'template-parts/site/the-content');
        }

        if ( get_post_type( get_the_ID() ) == 'post' ) {
          get_template_part( 'template-parts/site/share-dialog-bot');
        }

        get_template_part( 'template-parts/singles/post/meta', 'post__bot' );
        ?>

      </div>

    </div>

	</div>

  <?php
  get_template_part('template-parts/acf/queries/row--posts__rel' );
  ?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
