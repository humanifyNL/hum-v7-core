<?php
/**
 * Query posts - isotope
 *
 * @package hum-v7-core
 */

$main_id = get_the_id(); // page id for template parts

if ( have_posts() ) {

  // enable iso manually
  // block will add iso class based on this variable
  $enable_iso = true;

  if ( $enable_iso ) {
    hum_get_query_buttons_iso( $wp_query, 'category' );
    echo '<div class="grid--previews '.hum_grid_preview().' grid--iso">';
  } else {
    echo '<div class="grid--previews '.hum_grid_preview().'">';
  }

    while ( have_posts() ) {

      the_post();
      include( locate_template( 'template-parts/singles/post/preview-post.php' ) );

    }

  echo '</div>';

}
