<?php
/**
 * The default template for displaying post content
 *
 * @package hum-v7-core
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php
  get_template_part( 'template-parts/singles/post/header', 'post' );
  ?>

  <div class="post-content">

    <?php
    // content
    if ( have_rows( 'flex_content_post' ) ) {
      get_template_part( 'template-parts/acf/flex-post' );
    } else {
      get_template_part( 'template-parts/site/the-content');
    }
    ?>

	</div>


</article><!-- #post-<?php the_ID(); ?> -->

<?php

include( locate_template('template-parts/acf/queries/row--posts__rel.php') );
