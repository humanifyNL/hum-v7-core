/**
 * Isotope init
 *
 * @package hum-v7-core
 */

jQuery(document).ready(function($) {

  // init Isotope
  var $grid = $('.grid--iso').isotope({
    itemSelector: '.block--iso',
    layoutMode: 'fitRows',
    // transitionDuration: 0
  });

  var $grid_2 = $('.grid--iso__instant').isotope({
    itemSelector: '.block--iso',
    layoutMode: 'fitRows',
    filter: '.iso-initial',
    transitionDuration: 0
  });

  // bind filter button click
  $('.button-group--iso').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    $grid.isotope({ filter: filterValue });
    $grid_2.isotope({ filter: filterValue });
  });

  // change is-checked class on buttons
  $('.button-group--iso').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this ).addClass('is-checked');
    });
  });
});
