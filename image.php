<?php
/**
 * The template for displaying image attachments
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) {

	      the_post();

	      ?>
	      <article id="post-<?php the_ID(); ?>" <?php post_class( 'image-attachment' ); ?><?php hum_core_semantics( 'post' ); ?>>

	        <div class="post-attachment wrap">

	          <?php
	          /**
						 * Filter the default image attachment size.
	           * @param string $image_size Image size. To retrieve 'full' image size, use:
	           * $image_size = apply_filters( 'hum_base_attachment_size', 'full' );
	           * custom size = apply_filters( 'hum_base_attachment_size', array( 960, 960 ) );
						 */
	          $image_size = apply_filters( 'hum_base_attachment_size', 'full' );

	          echo wp_get_attachment_image( get_the_ID(), $image_size );

	          hum_excerpt( 'entry-caption' );
	          ?>

	        </div>

	        <header class="post-header wrap">

	          <?php
	          the_title( '<h1 class="post-title"', '</h1>' );
	          ?>
	          <link href="<?php the_permalink(); ?>"/>

	        </header>

	        <div class="post-content">

	          <div class="entry-description wrap">

	            <?php
							the_content(); // Display image description field content.
							?>

	          </div>

	          <?php
						hum_page_nav();
						?>

	        </div>

	        <footer class="post-meta wrap">

	          <?php get_template_part( 'template-parts/singles/post/meta', 'post__bot' ); ?>

	        </footer>

	      </article>

	      <nav id="image-navigation" class="navigation image-navigation wrap">

	        <div class="nav-links">

	          <div class="nav-previous"><?php previous_image_link( false, esc_html__( 'Vorige', 'hum-base' ) ); ?></div>
	          <div class="nav-next"><?php next_image_link( false, esc_html__( 'Volgende', 'hum-base' ) ); ?></div>

	        </div>

	      </nav>
	      <?php

	    }
			?>

	    <section class="attachment-gallery gallery">

	      <?php
	      // https://wpsmackdown.com/wordpress-display-all-images-attached-to-post-page/
	      // https://developer.wordpress.org/reference/functions/get_attached_media/
	      $images = get_attached_media('image', $post->ID);

	      foreach ( $images as $image ) {

	        $ximage = wp_get_attachment_image_src($image->ID,'medium');
	        $xurl = wp_get_attachment_url($image->ID);

	        echo '<div class="gallery-icon portrait"><a href="' .$xurl[0] . '"><img src="' .$ximage[0] . '" /></a></div';
	      }
	      ?>

	    </section>

		</main>

	</div>

</div>

<?php
get_footer();
