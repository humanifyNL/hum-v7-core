<?php
/**
 * Hum Base WP image sizes
 *
 * @package hum-v7-core
 */

/**
 * Enable support for Post Thumbnails on posts and pages.
 *
 */
add_theme_support( 'post-thumbnails' );

/**
 * Add new image size for custom post/page headers and select default header image.
 *
 */
add_image_size( 'featured', 1920, 9999 ); // Unlimited height, soft crop
add_image_size( 'small', 360, 360 ); // Unlimited height, hard crop
add_image_size( 'featured-sq', 640, 640 ); // Unlimited height, hard crop
add_image_size( 'admin', 100, 80 ); // Unlimited height, hard crop

/**
 * This theme supports standard custom logo.
 * With logo feature being active, site BODY gets new class .wp-custom-logo.
 *
 */
add_theme_support( 'custom-logo', array(
  'height'      => null, // Allow full flexibility if no size is specified.
  'width'       => null, // Allow full flexibility if no size is specified.
  'flex-height' => true,
  'flex-width'  => true,
) );


/**
 * Add new sizes to media library
 * http://www.paulund.co.uk/custom-image-sizes-media-library
 */
function hum_core_custom_sizes( $sizes ) {

    return array_merge( $sizes, array(

        'small' => __( 'Small' ),
    ) );
}

add_filter( 'image_size_names_choose', 'hum_core_custom_sizes' );


/**
 * Remove wp 5.3 new image sizes
 *
 * https://wordpress.stackexchange.com/questions/354378/wordpress-adding-scaled-images-that-dont-exist-1536x1536-and-2048x2048
 */
function hum_core_remove_default_image_sizes( $sizes) {

    unset( $sizes['medium_large']);
    unset( $sizes['1536x1536']);
    unset( $sizes['2048x2048']);

    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'hum_core_remove_default_image_sizes');
