<?php
/**
 * Query functions
 *
 * @package hum-v7-core
 */

// index post
function hum_core_loop( $query ) {

  if ( $query->is_main_query() ) {

    // home
    if( !is_admin() && is_home() && !is_front_page() ) {

      $query->set( 'order', 'DESC' );
      $query->set( 'orderby', 'date' );
    }

  }

}
add_action( 'pre_get_posts', 'hum_core_loop');
