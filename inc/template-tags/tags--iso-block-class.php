<?php
/**
 * Hum Base isotope classes for blocks
 * $taxonomy = any valid tax
 *
 * @package hum-v7-core
 */

 if ( !function_exists( 'hum_iso_class' ) ) {

   function hum_iso_class( $taxonomy ) {

     global $post;
     $get_terms = get_the_terms( $post->ID, $taxonomy);

     if ( $get_terms && !is_wp_error( $get_terms ) ) {

       $cat_links = array();
       $cat_links[] = 'block--iso';

       foreach ( $get_terms as $term ) {
         $cat_links[] = $term->slug;
       }
       $iso_block_class = join( " ", $cat_links );

       // print the category name(s)
       echo $iso_block_class;
     }
   }
 }
