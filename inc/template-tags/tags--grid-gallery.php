<?php
/**
 * Grid gallery class modifier
 *
 * ACF field: group_5f0990f32e941
 *
 * @package hum-v7-core
 */

if ( !function_exists( 'hum_grid_gallery' ) ) {

  function hum_grid_gallery() {

    $grid_gallery = get_sub_field( 'grid_gallery' );

    if ( isset($grid_gallery) ) {

      switch ( $grid_gallery ) {
        case 'grid-5':
          $grid = 'grid--gallery grid--20';
          break;
        case 'grid-4':
          $grid = 'grid--gallery grid--25';
          break;
        case 'grid-3':
          $grid = 'grid--gallery grid--33';
          break;
        case 'grid-2':
          $grid = 'grid--gallery grid--50';
          break;
      }

      return $grid;
    }
  }
}

/* ACF populate select field
 *
 * https://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 * fieldname = grid_gallery
 */

function acf_load_grid_gallery_field_choices( $field ) {

    // reset choices
    $field['choices'] = array(
      'grid-5' => '5 per rij',
      'grid-4' => '4 per rij',
      'grid-3' => '3 per rij',
      'grid-2' => '2 per rij',
    );

    // return the field
    return $field;

}

add_filter('acf/load_field/name=grid_gallery', 'acf_load_grid_gallery_field_choices');
