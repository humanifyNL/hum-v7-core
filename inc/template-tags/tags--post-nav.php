<?php
/**
 * Custom post navigation
 *
 * @package hum-v7-core
 */

if ( ! function_exists( 'hum_post_nav_tag' ) ) {

  function hum_post_nav( $tax = 'category' ) {

    $prev_post = get_previous_post( true, "", $tax );
    $next_post = get_next_post( true, "", $tax );

    if ( !empty($prev_post) || !empty($next_post) ) {

      echo '<nav class="navigation post-navigation " role="navigation">';

        echo '<div class="nav-links wrap';
          if ( $prev_post && !$next_post) { echo ' has-prev'; }
          if ( $next_post && !$prev_post) { echo ' has-next'; }
          if ( $next_post && $prev_post) { echo ' has-prev-next'; }
        echo '">';

          if ( !empty( $prev_post ) ) {

            $prev_title = $prev_post->post_title;
            $prev_id = $prev_post->ID;
            $prev_url = get_permalink($prev_id);

            echo '<div class="nav-previous">';

              echo '<a class="block__link" href="'.$prev_url.'" rel="previous">';

                // link
                echo '<span class="block__linktext">'.$prev_title.'</span>';

              echo '</a>';

            echo '</div>';
          }

          if ( !empty( $next_post ) ) {

            $next_title = $next_post->post_title;
            $next_id = $next_post->ID;
            $next_url = get_permalink($next_id);

            echo '<div class="nav-next">';

              echo '<a class="block__link" href="'.$next_url.'" rel="next">';

                // link
                echo '<div class="block__linktext">'.$next_title.'</div>';

              echo '</a>';

            echo '</div>';
          }

        echo '</div>';

      echo '</nav>';
    }
  }
}


if ( ! function_exists( 'hum_post_nav_thumb' ) ) {

  function hum_post_nav_thumb( $tax = 'category') {

    $prev_post = get_previous_post( true, "", $tax );
    $next_post = get_next_post( true, "", $tax );

    if ( !empty($prev_post) || !empty($next_post) ) {

      echo '<nav class="navigation post-navigation" role="navigation">';

        echo '<div class="nav-links nav-links__thumb wrap';
          if ( $prev_post && !$next_post) { echo ' has-prev'; }
          if ( $next_post && !$prev_post) { echo ' has-next'; }
          if ( $next_post && $prev_post) { echo ' has-prev-next'; }
        echo '">';

          if ( !empty( $prev_post ) ) {

            $prev_title = $prev_post->post_title;
            $prev_id = $prev_post->ID;
            $prev_url = get_permalink($prev_id);
            $prev_img = get_the_post_thumbnail( $prev_id , 'thumbnail' );

            // custom field img
            // $prev_img = get_field( 'products_img_thumb', $prev_id );
            // $prev_size = 'thumbnail';
            // $prev_img_id = $prev_img['id'];

            echo '<div class="nav-previous">';

              echo '<a class="block__link" href="'.$prev_url.'" rel="previous">';

                // img
                echo '<div class="block__img">';
                echo $prev_img;
                // custom field img
                // echo wp_get_attachment_image( $prev_img_id, $prev_size );
                echo '</div>';

                // link
                echo '<div class="block__linktext">'.$prev_title.'</div>';

              echo '</a>';

            echo '</div>';
          }

          if ( !empty( $next_post ) ) {

            $next_title = $next_post->post_title;
            $next_id = $next_post->ID;
            $next_url = get_permalink($next_id);
            $next_img = get_the_post_thumbnail( $next_id, 'thumbnail' );
            // custom field img
            // $next_img = get_field( 'products_img_thumb', $next_id );
            // $next_size = 'thumbnail';
            // $next_img_id = $next_img['id'];

            echo '<div class="nav-next">';

              echo '<a class="block__link" href="'.$next_url.'">';

                // link
                echo '<div class="block__linktext">'.$next_title.'</div>';

                // img
                echo '<div class="block__img">';
                echo $next_img;
                // custom field img
                // echo wp_get_attachment_image( $next_img_id, $next_size );
                echo '</div>';

              echo '</a>';

            echo '</div>';
          }

        echo '</div>';

      echo '</nav>';
    }
  }
}
