<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package hum-v7-core
 */

/**
 * Load the core template-tags parts
 */
require_once ( get_template_directory() . '/inc/template-tags/tags--meta.php' );
require_once ( get_template_directory() . '/inc/template-tags/tags--comments.php' );
require_once ( get_template_directory() . '/inc/template-tags/tags--branding.php' );
require_once ( get_template_directory() . '/inc/template-tags/tags--excerpt.php' );
require_once ( get_template_directory() . '/inc/template-tags/tags--page-nav.php' );
require_once ( get_template_directory() . '/inc/template-tags/tags--post-nav.php' );

// get term buttons for isotope
include_once ( get_template_directory() . '/inc/template-tags/tags--iso-get-buttons.php' );
// get terms in block class (for isotope)
include_once ( get_template_directory() . '/inc/template-tags/tags--iso-block-class.php' );
// several utility functions / new ideas
include_once ( get_template_directory() . '/inc/template-tags/tags--utility.php' );
// post block elements
include_once ( get_template_directory() . '/inc/template-tags/tags--image-default.php' );

/**
 * ACF tags & modifiers
 */
// button class function
include_once ( get_template_directory() . '/inc/template-tags/tags--button-class.php' );
// grid modifiers
include_once ( get_template_directory() . '/inc/template-tags/tags--grid-preview.php' );
include_once ( get_template_directory() . '/inc/template-tags/tags--grid-section.php' );
include_once ( get_template_directory() . '/inc/template-tags/tags--grid-gallery.php' );
// style modifiers
include_once ( get_template_directory() . '/inc/template-tags/tags--style-row.php' );
include_once ( get_template_directory() . '/inc/template-tags/tags--style-list.php' );
include_once ( get_template_directory() . '/inc/template-tags/tags--style-block.php' );

/**
* JavaScript Detection.
* Adds a `js` class to the root `<html>` element when JavaScript is detected.
*
*/
function hum_base_javascript_detection() {

	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action( 'wp_head', 'hum_base_javascript_detection', 0 );


/**
* Hide some archive template descriptions. Descriptions are still available for accessibility purposes.
* They will be replaced with web icons (inserted in category.php, etc.).
*
*/
function hum_base_modify_archive_title( $title ) {

	if ( is_category() ) {

		$title = sprintf( '<span class="screen-reader-text">%1$s </span>%2$s',
		__( 'Category:', 'hum-base' ),
		single_cat_title( '', false )
		);

	} elseif ( is_tag() ) {

		$title = sprintf( '<span class="screen-reader-text">%1$s </span>%2$s',
		__( 'Tag:', 'hum-base' ),
		single_tag_title( '', false )
		);

	} elseif ( is_author() ) {

		$title = sprintf( '<span class="screen-reader-text">%1$s </span>%2$s',
		__( 'Author:', 'hum-base' ),
		'<span class="vcard">' . get_the_author() . '</span>'
		);
	}

	return $title;
}
add_filter( 'get_the_archive_title', 'hum_base_modify_archive_title' );


/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 *
 */
if ( ! function_exists( 'hum_base_pingback_header' ) ) {

	function hum_base_pingback_header() {

		if ( is_singular() && pings_open() ) {

			echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
		}

	}
	add_action( 'wp_head', 'hum_base_pingback_header' );
}
