<?php
/**
 * The main template file and used for blog-index,
 * unless there is a dedicated template for it
 *
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package hum-v7-core
 */

get_header();
?>

<div class="wrap-main">

	<div id="primary" class="content-area">

		<main id="main" class="site-main">

			<div class="page-content">

				<?php
			  if ( have_posts() && is_home() && !is_front_page() ) {

					?>
					<section class="row row--previews">

				  	<div class="block-body wrap">

							<div class="grid--previews <?php echo hum_grid_preview();?>">

								<?php
								while ( have_posts() ) {

									the_post();
									get_template_part( 'template-parts/singles/post/preview', 'post' );

								}
								?>

						  </div>
					  </div>

						<?php
						hum_archive_page_nav();
						?>

					</section>
					<?php
				}
				?>
			</div>

    </main>

	</div>

</div>

<?php
get_footer();
